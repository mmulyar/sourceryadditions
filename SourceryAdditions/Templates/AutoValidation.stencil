// MARK: - AutoValidation
import Foundation
import Validator
import SourceryAdditions
{% for type in types.all|annotated:"AutoImport" %}
{% for value in type.annotations.AutoImport|toArray %}
import {{ value }}
{% endfor %}
{% endfor %}

extension ValidationResult {
    var errors: [Error]? {
        switch self {
        case .invalid(let errors):
            return errors
        default:
            return nil
        }
    }
    var error: Error? {
        switch self {
        case .invalid(let errors):
            return errors.first
        default:
            return nil
        }
    }
}


enum AutoValidationPattern: String, ValidationPattern {

    case noSpacesNewLines

    public var pattern: String {
        switch self {
        case .noSpacesNewLines: return "^\\S*$"
        }
    }
}


enum ValidationError: String, Error, AutoEquatable {
    case min = "Value is too small"
    case max = "Value is too long"
    case patternMatch = "Value does not match"
    case empty = "Value is empty"
    case equality = "Values are not equal"
    case difference = "Values should be different"

    var message: String {
        return self.rawValue
    }
}

{% for type in types.implementing.AutoValidation %}
// MARK: {{type.name}} AutoValidation
extension {{type.name}} {

    enum ValidationField: Int {
        {% for var in type.variables %}
        case {{var.name}}
        {% endfor %}
    }

    struct ValidationErrorContainer: Error {
        let field: ValidationField
        let error: ValidationError
    }
    
    {% for var in type.variables %}
    func {{var.name}}ValidationRules() -> ValidationRuleSet<{{var.unwrappedTypeName}}> {
        var rules = ValidationRuleSet<{{var.unwrappedTypeName}}>()
        {% if var|annotated:"validationNotEmpty" %}
        let rule{{"ValidationNotEmpty"}} = ValidationRuleRequired<{{var.unwrappedTypeName}}>(error: ValidationErrorContainer(field: .{{var.name}}, error: .empty))
        rules.add(rule: rule{{ "ValidationNotEmpty" }})
        {% endif %}
        {% if var|annotated:"validationMin" and var.unwrappedTypeName == "String" %}
        let rule{{"ValidationMin"}} = ValidationRuleLength(min: {{ var.annotations.validationMin }}, error: ValidationErrorContainer(field: .{{var.name}}, error: .min))
        rules.add(rule: rule{{ "ValidationMin" }})
        {% endif %}
        {% if var|annotated:"validationMax" and var.unwrappedTypeName == "String" %}
        let rule{{"ValidationMax"}} = ValidationRuleLength(max: {{ var.annotations.validationMax }}, error: ValidationErrorContainer(field: .{{var.name}}, error: .max))
        rules.add(rule: rule{{ "ValidationMax" }})
        {% endif %}
        {% if var|annotated:"validationMin" %}{% if var|implements:"Comparable" or var.unwrappedTypeName == "Int" or var.unwrappedTypeName == "Double" or var.unwrappedTypeName == "Float" %}
        let rule{{"ValidationMin"}} = ValidationRuleOptionalComparison(min: {{ var.annotations.validationMin }}, max: nil, error: ValidationErrorContainer(field: .{{var.name}}, error: .min))
        rules.add(rule: rule{{ "ValidationMin" }})
        {% endif %}{% endif %}
        {% if var|annotated:"validationMax" %}{% if var|implements:"Comparable" or var.unwrappedTypeName == "Int" or var.unwrappedTypeName == "Double" or var.unwrappedTypeName == "Float" %}
        let rule{{"ValidationMax"}} = ValidationRuleOptionalComparison(min: nil, max: {{ var.annotations.validationMax }}, error: ValidationErrorContainer(field: .{{var.name}}, error: .max))
        rules.add(rule: rule{{ "ValidationMax" }})
        {% endif %}{% endif %}
        {% if var|annotated:"validationEmail" %}
        let rule{{"ValidationEmail"}} = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationErrorContainer(field: .{{var.name}}, error: .patternMatch))
        rules.add(rule: rule{{ "ValidationEmail" }})
        {% endif %}
        {% if var|annotated:"validationNoSpacesNoNewLines" %}
        let rule{{"ValidationNoSpacesNoNewLines"}} = ValidationRulePattern(pattern: AutoValidationPattern.noSpacesNewLines, error: ValidationErrorContainer(field: .{{var.name}}, error: .patternMatch))
        rules.add(rule: rule{{ "ValidationNoSpacesNoNewLines" }})
        {% endif %}
        {% if var|annotated:"validationEqualTo" %}
        let rule{{"ValidationEqualTo"}}{{ var.annotations.validationEqualTo|upperFirstLetter }} = ValidationRuleOptionalEquality(target: self.{{ var.annotations.validationEqualTo }}, error: ValidationErrorContainer(field: .{{var.name}}, error: .equality))
        rules.add(rule: rule{{"ValidationEqualTo"}}{{ var.annotations.validationEqualTo|upperFirstLetter }})
        {% endif %}
        {% if var|annotated:"validationNotEqualTo" %}
        var rule{{"ValidationNotEqualTo"}}{{ var.annotations.validationNotEqualTo|upperFirstLetter }} = ValidationRuleOptionalEquality(target: self.{{ var.annotations.validationNotEqualTo }}, error: ValidationErrorContainer(field: .{{var.name}}, error: .difference))
        rule{{"ValidationNotEqualTo"}}{{ var.annotations.validationNotEqualTo|upperFirstLetter }}.shouldBeEqual = false
        rules.add(rule: rule{{"ValidationNotEqualTo"}}{{ var.annotations.validationNotEqualTo|upperFirstLetter }})
        {% endif %}
        return rules
    }
    {% endfor %}
	
    func validate() -> ValidationResult {
    
        var result: ValidationResult = .valid
    
        {% for var in type.variables %}
        result = result.merge(with: Validator.validate(input: {{var.name}}, rules: {{var.name}}ValidationRules()))
        {% endfor %}
    
        return result
    }
}

extension {{type.name}}.ValidationErrorContainer: Equatable {}
internal func == (lhs: {{type.name}}.ValidationErrorContainer, rhs: {{type.name}}.ValidationErrorContainer) -> Bool {
    guard lhs.field.rawValue == rhs.field.rawValue else { return false }
    guard lhs.error.rawValue == rhs.error.rawValue else { return false }
    return true
}
{% endfor %}

public struct ValidationRuleOptionalEquality<T: Equatable>: ValidationRule {

    public typealias InputType = T
    public let error: Error
    /**
     A value to compare an input against.
     */
    let target: T?
    /**
     A closure that returns a value to compare an input against.
     */
    let dynamicTarget: (() -> T?)?

    /**
     Initializes a `ValidationRuleOptionalEquality` with a value to compare an input
     against, and an error describing a failed validation.
     - Parameters:
        - target: A value to compare an input against.
        - error: An error describing a failed validation.
     */
    public init(target: T?, error: Error) {
        self.target = target
        self.error = error
        self.dynamicTarget = nil
    }

    var shouldBeEqual = true

    /**
     Initializes a `ValidationRuleOptionalEquality` with a closure that returns a value
     to compare an input against, and an error describing a failed validation.
     - Parameters:
     - target: A closure that returns a value to compare an input against.
     - error: An error describing a failed validation.
     */
    public init(dynamicTarget: @escaping (() -> T?), error: Error) {
        self.dynamicTarget = dynamicTarget
        self.error = error
        self.target = nil
    }

    /**
     Validates the input.
     - Parameters:
        - input: Input to validate.
     - Returns:
     true if the input equals the target.
     */
    public func validate(input: T?) -> Bool {
        var isEqual = false
        if let dynamicTarget = dynamicTarget {
            isEqual = input == dynamicTarget()
        } else if let target = target {
            isEqual = input == target
        } else {
            isEqual = false
        }

        return isEqual == shouldBeEqual
    }
}


public struct ValidationRuleOptionalComparison<T: Comparable>: ValidationRule {

    public typealias InputType = T

    public let error: Error

    /**

     A minimum value an input must equal to or be greater than to pass as valid.

     */
    let min: T?

    /**

     A maximum value an input must equal to or be less than to pass as valid.

     */
    let max: T?

    /**

     Initializes a `ValidationRuleComparison` with a min and max value for an
     input comparison, and an error describing a failed validation.

     - Parameters:
        - min: A minimum value an input must equal to or be greater than.
        - max: A maximum value an input must equal to or be less than.
        - error: An error describing a failed validation.

     */
    public init(min: T?, max: T?, error: Error) {
        self.min = min
        self.max = max
        self.error = error
    }

    /**

     Validates the input.

     - Parameters:
        - input: Input to validate.

     - Returns:
        true if the input is equal to or between the minimum and maximum.

     */
    public func validate(input: T?) -> Bool {
        guard let input = input else { return false }
        if let min = min, input < min { return false }
        if let max = max, input > max { return false }
        return true
    }

}
